<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */
Route::post('login', 'userController@login');
Route::post('register', 'userController@register');
Route::group(['middleware' => 'auth:api'], function(){
    Route::get('/blog', 'blog@index');
    Route::get('/blog/{id}', 'blog@show');
    Route::post('/blog', 'blog@store');
    Route::put('/blog/{id}', 'blog@update');
    Route::delete('/blog/{id}', 'blog@destroy');
    //-----------------------------------------
    Route::get('/status', 'statusController@index');
    Route::post('/status', 'statusController@store');
});