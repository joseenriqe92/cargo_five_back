<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use DB;

class userController extends Controller
{
    public function login(Request $request){ 
        
            $request->validate([
                'email'       => 'required|string|email',
                'password'    => 'required|string',
            ]);
            $credentials = request(['email', 'password']);
            if (!Auth::attempt($credentials)) {
                return response()->json([
                    'message' => 'Unauthorized'], 401);
            }
            $user = $request->user();
            $tokenResult = $user->createToken('CargoFive');
            $token = $tokenResult->token;
            return ['accessToken' => $tokenResult->accessToken, 'user' => $user,'code' => 200];
        
        
    }
    public function register(Request $request) 
    { 
        try {
            //code...
            $validator = Validator::make($request->all(), [ 
                'name' => 'required', 
                'email' => 'required|email', 
                'password' => 'required', 
            ]); 
            $input = $request->all(); 
                    $input['password'] = bcrypt($input['password']); 
                    $user = user::create($input);
            return ['message' => 'user created','code' => 200];;
            // return response()->json(['success'=>$success], $this-> successStatus);
        } catch (\Throwable $th) {
            //throw $th;
            echo $th;
        }
         
    }
}
