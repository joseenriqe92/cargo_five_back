<?php

namespace App\Http\Controllers;
use Carbon\Carbon;

use Illuminate\Http\Request;
use DB;

class blog extends Controller
{
    public function index() {
      try {
         
         $getBlog = DB::table('blog')
            ->join('status', 'blog.status_id', '=', 'status.id')
            ->where(['blog.is_available' => 'true'])
            ->select('blog.id as blog_id', '*')
            ->get();
      return $getBlog;
      } catch (\Throwable $th) {
         echo $th;
         return ['message' => 'Error in fetching data.','code' => 500];
      }
     }
     public function store(Request $request) { 
        try {
            $title = $request->input('title');
            $content = $request->input('content');
            $status_id = $request->input('status_id');
               DB::table('blog')->insert(
                  ['title' => $title, 'content' => $content, 'status_id' => $status_id, 'created_at' => Carbon::now('America/Caracas'), 'is_available' => 'true']
               );
            return ['message' => 'Data inserting successfully.','code' => 200];
        } catch (\Throwable $th) {
            // throw $th;
            echo $th;
            return ['message' => 'Error inserting data.','code' => 500];
        };
    }
     public function show($id) {
         try {
            $getBlog = DB::table('blog')
                  ->join('status', 'blog.status_id', '=', 'status.id')
                  ->where(['blog.id' => $id, 'blog.is_available' => 'true']) 
                  ->get();
            return $getBlog;
         } catch (\Throwable $th) {
            //throw $th;
            return ['message' => 'An error occurred.','code' => 500];
         }
     }
     public function update(Request $request, $id) {
         try {
            $title = $request->input('title');
            $content = $request->input('content');
            $status_id = $request->input('status_id');
            DB::table('blog')
               ->where(['blog.id' => $id])
               ->update(['title' => $title, 'content' => $content, 'status_id' => $status_id, 'updated_at' => Carbon::now('America/Caracas')]);
            return ['message' => 'Data updated successfully.','code' => 200];
         } catch (\Throwable $th) {
            return ['message' => 'Error inserting data.','code' => 500];
         }  
     }
     public function destroy($id) {
        try {
            DB::table('blog')
               ->where(['blog.id' => $id])
               ->update(['is_available' => 'false']);
            return ['message' => 'Data deleted successfully.','code' => 500]; 
        } catch (\Throwable $th) {
            return ['message' => 'Error deleting data.','code' => 500]; 
        }
     }
}
